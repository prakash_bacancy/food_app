class HomesController < ApplicationController
  def index
  end

  def about_us
  end

  def contact_us
  end

  def home
  end

  def careers
  end
end
